package captcha

import (
	"context"
	"github.com/mojocn/base64Captcha"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/redis"
	"image/color"
	"time"
)

type Conf struct {
	Length int    `json:",optional,default=5,env=CAPTCHA_KEY_LONG"`                                       // captcha length
	Width  int    `json:",optional,default=240,env=CAPTCHA_IMG_WIDTH"`                                    // captcha width
	Height int    `json:",optional,default=80,env=CAPTCHA_IMG_HEIGHT"`                                    // captcha height
	Driver string `json:",optional,default=digit,options=[digit,string,math,chinese],env=CAPTCHA_DRIVER"` // captcha type
}

// RedisStore redis保存验证码 实现 base64Captcha.Store
type RedisStore struct {
	Prefix     string
	Expiration time.Duration
	Context    context.Context
	Redis      *redis.Redis
}

func NewRedisStore(r *redis.Redis) *RedisStore {
	return &RedisStore{
		Prefix:     "CAPTCHA_",
		Expiration: time.Minute * 5,
		Redis:      r,
	}
}

// UseWithCtx add context for captcha.
func (s *RedisStore) UseWithCtx(ctx context.Context) base64Captcha.Store {
	s.Context = ctx
	return s
}

func (s *RedisStore) Set(id string, value string) error {
	err := s.Redis.Setex(s.Prefix+id, value, int(s.Expiration.Seconds()))
	if err != nil {
		logx.Errorw("error occurs when captcha key sets to redis", logx.Field("detail", err))
		return err
	}
	return nil
}

func (s *RedisStore) Get(id string, clear bool) string {
	key := s.Prefix + id
	val, err := s.Redis.Get(key)
	if err != nil {
		logx.Errorw("error occurs when captcha key gets from redis", logx.Field("detail", err))
		return ""
	}
	defer func() {
		if clear {
			_, err := s.Redis.Del(key)
			if err != nil {
				logx.Errorw("error occurs when captcha key deletes from redis", logx.Field("detail", err))
				return
			}
		}
	}()
	return val
}

func (s *RedisStore) Verify(id, answer string, clear bool) bool {
	value := s.Get(id, clear)
	return value == answer
}

func NewCaptcha(c Conf, r *redis.Redis) *base64Captcha.Captcha {

	driver := NewDriver(c)
	store := NewRedisStore(r)

	return base64Captcha.NewCaptcha(driver, store)
}

func NewDriver(c Conf) base64Captcha.Driver {
	var driver base64Captcha.Driver

	bgColor := &color.RGBA{
		R: 254,
		G: 254,
		B: 254,
		A: 254,
	}

	fonts := []string{
		"ApothecaryFont.ttf",
		"DENNEthree-dee.ttf",
		"Flim-Flam.ttf",
		"RitaSmith.ttf",
		"actionj.ttf",
		"chromohv.ttf",
	}

	switch c.Driver {
	case "digit":
		// 生成数字验证码
		driver = base64Captcha.NewDriverDigit(c.Height, c.Width, c.Length, 0.7, 80)
	case "string":
		driver = base64Captcha.NewDriverString(c.Height, c.Width, 12, 3, c.Length, "qwertyupasdfghjkzxcvbnm23456789", bgColor, nil, fonts)
	case "math":
		driver = base64Captcha.NewDriverMath(c.Height, c.Width, 12, 3, bgColor,
			nil, fonts)
	case "chinese":
		driver = base64Captcha.NewDriverChinese(c.Height, c.Width, 10, 4, c.Length,
			"天地玄黄宇宙洪荒日月盈辰宿列张寒来暑往秋收冬藏闰余成岁律吕调阳夫大水浸灌草木破坡石右云虫师军舰流浪数据速度", bgColor, nil,
			[]string{"wqy-microhei.ttc"})
	default:
		driver = base64Captcha.NewDriverDigit(c.Height, c.Width,
			c.Length, 0.7, 80)
	}

	return driver
}
