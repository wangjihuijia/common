package utils

import "testing"

func TestOrderNum(t *testing.T) {
	type args struct {
		prefix string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "one", args: args{
			prefix: "TF",
		}, want: "two"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := OrderNum(tt.args.prefix); got != tt.want {
				t.Errorf("OrderNum() = %v, want %v", got, tt.want)
			}
		})
	}
}
