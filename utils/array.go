package utils

// DelStringByElement 删除数组中的元素
func DelStringByElement(array []string, element string) ([]string, error) {
	var j int
	for _, e := range array {
		if e != element {
			array[j] = e
			j++
		}
	}
	return array[:j], nil
}
