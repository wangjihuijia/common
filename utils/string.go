package utils

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// RandStringRunes 随机字符串
func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

var r *rand.Rand

func init() {
	r = rand.New(rand.NewSource(time.Now().Unix()))
}

// RandString 生成随机大写字符串
func RandString(len int) string {
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		b := r.Intn(26) + 65
		bytes[i] = byte(b)
	}
	return string(bytes)
}

// RandInt 生成随机数字字符串
func RandInt(len int) string {
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		b := r.Intn(9) + 48
		bytes[i] = byte(b)
	}
	return string(bytes)
}

// 生成单号
func OrderNum(prefix string) string {

	datePart := time.Now().Format("060102")

	str := strconv.FormatInt(time.Now().Unix(), 10)

	datePart2 := str[len(str)-5 : len(str)]
	// 生成随机数部分（4位随机数）
	rand.Seed(time.Now().UnixNano())
	randomPart := fmt.Sprintf("%07d", rand.Intn(10000000))

	// 最终的订单号由前缀、日期和随机数部分组成
	orderNumber := fmt.Sprintf("%s%s%s%s", prefix, datePart, datePart2, randomPart)
	return orderNumber
}
