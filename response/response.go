package response

import (
	"context"

	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc/status"
	"net/http"

	"gitee.com/wangjihuijia/common/errorx"
	"github.com/zeromicro/go-zero/rest/httpx"
)

type Body struct {
	Code    uint32      `json:"code"`
	Message string      `json:"msg"`
	Data    interface{} `json:"data,omitempty"`
	Trace   string      `json:"trace,omitempty"`
	Span    string      `json:"span,omitempty"`
}

func HttpResult(r *http.Request, w http.ResponseWriter, resp interface{}, err error) {
	var body Body
	var traceID, spanID string

	if err == nil {
		body.Code = 200
		body.Message = "success"
		body.Data = resp

		httpx.OkJson(w, body)
	} else {

		//错误返回
		errcode := errorx.ServerCommonError
		errmsg := "服务器开小差啦，稍后再来试一试"

		causeErr := errors.Cause(err)
		var e *errorx.CodeError
		if errors.As(causeErr, &e) { // 自定义错误类型

			// 自定义CodeError
			errcode = e.GetErrCode()
			errmsg = e.GetErrMsg()
			if e.GetErrCode() == errorx.BizInfoError {
				httpx.WriteJson(w, http.StatusOK, Body{
					Code:    e.GetErrCode(),
					Message: e.GetErrMsg(),
				})
				return
			}

			logx.WithContext(r.Context()).Errorf("【API-ERR】 : %+v ", err)

			traceID = TraceIDFromContext(r.Context())
			spanID = SpanIDFromContext(r.Context())
		} else {
			if grpcStatus, ok := status.FromError(causeErr); ok { // grpc err错误
				grpcCode := uint32(grpcStatus.Code())

				if errorx.IsCodeErr(grpcCode) { // 区分自定义错误跟系统底层、db等错误，底层、db错误不能返回给前端
					errcode = grpcCode
					errmsg = grpcStatus.Message()
				}
			}

			traceID = TraceIDFromContext(r.Context())
			spanID = SpanIDFromContext(r.Context())
			logx.WithContext(r.Context()).Errorf("【API-ERR】 : %+v ", err)
		}

		httpx.WriteJson(w, http.StatusBadRequest, Body{
			Code:    errcode,
			Message: errmsg,
			Trace:   traceID,
			Span:    spanID,
		})

	}

}

func SpanIDFromContext(ctx context.Context) string {
	spanCtx := trace.SpanContextFromContext(ctx)
	if spanCtx.HasSpanID() {
		return spanCtx.SpanID().String()
	}

	return ""
}

func TraceIDFromContext(ctx context.Context) string {
	spanCtx := trace.SpanContextFromContext(ctx)
	if spanCtx.HasTraceID() {
		return spanCtx.TraceID().String()
	}

	return ""
}
