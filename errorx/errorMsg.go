package errorx

var message map[uint32]string

func init() {
	message = make(map[uint32]string)
	message[OK] = "SUCCESS"
	message[ServerCommonError] = "服务器开小差啦,稍后再来试一试"
	message[RequestParamError] = "参数错误"
	message[DbError] = "数据库繁忙,请稍后再试"
	message[TokenExpireError] = "token失效，请重新登陆"
	message[TokenGenerateError] = "生成token失败"
	message[TokenValidateError] = "token验证失败"
	message[DbQueryError] = "查询失败"
	message[DbLastInsertIdError] = "查询失败"
	message[DbInsertError] = "创建失败"
	message[DbNotFoundError] = "记录不存在"
	message[DbAlreadyExistsError] = "记录已经存在"
	message[ParamRequiredError] = "缺少参数"
	message[DbUpdateError] = "更新失败"
	message[SqlBuildError] = "sql构造错误"
	message[MarshalError] = "JSON 解析错误"
	message[TokenParseError] = "token解析错误"
	message[CacheExpireError] = "缓存已过期"
	message[CacheError] = "缓存错误"
	message[LoginTimeout] = "登录超时"
	message[SignedInElseWhere] = "在其他地方登录"
	message[RequiredTokenError] = "必须提供认证令牌"
	message[CaptchaError] = "验证码错误"
	message[CacheNotFound] = "缓存不存在"
	message[DbDeleteError] = "删除错误"
	message[RecordDisabled] = "已禁用"
	message[BizInfoError] = "业务错误"

}

func DefineErrMsg(codeErr *CodeError) {
	message[codeErr.errCode] = codeErr.errMsg
}

func MapErrMsg(errCode uint32) string {
	if msg, ok := message[errCode]; ok {
		return msg
	} else {
		return "服务器开小差啦,稍后再来试一试"
	}
}

func IsCodeErr(errCode uint32) bool {
	if _, ok := message[errCode]; ok {
		return true
	} else {
		return false
	}
}
