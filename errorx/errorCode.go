package errorx

import "fmt"

// 规则：业务编码(3位)+功能编码(3位)

const (
	OK                   uint32 = 200
	ServerCommonError    uint32 = 100001
	RequestParamError    uint32 = 100002
	DbError              uint32 = 100003
	TokenExpireError     uint32 = 100004
	TokenGenerateError   uint32 = 100005
	TokenValidateError   uint32 = 100006
	TokenParseError      uint32 = 100007
	DbQueryError         uint32 = 100008
	DbLastInsertIdError  uint32 = 100009
	DbInsertError        uint32 = 100010
	DbNotFoundError      uint32 = 100011
	DbAlreadyExistsError uint32 = 100012
	ParamRequiredError   uint32 = 100013
	DbUpdateError        uint32 = 100014
	SqlBuildError        uint32 = 100015
	MarshalError         uint32 = 100016
	CacheExpireError     uint32 = 100017
	CacheError           uint32 = 100018
	LoginTimeout         uint32 = 100019
	SignedInElseWhere    uint32 = 100020
	RequiredTokenError   uint32 = 100021
	CaptchaError         uint32 = 100022
	CacheNotFound        uint32 = 100023
	DbDeleteError        uint32 = 100024
	RecordDisabled       uint32 = 100025

	// BizInfoError 业务错误
	BizInfoError uint32 = 1010000
)

var (
	ServerCommonErr      = NewErrCodeMsg(ServerCommonError, "系统错误")
	RequestParamErr      = NewErrCodeMsg(RequestParamError, "参数错误")
	TokenExpireErr       = NewErrCodeMsg(TokenExpireError, "Token过期")
	TokenGenerateErr     = NewErrCodeMsg(TokenGenerateError, "Token生成错误")
	TokenParseErr        = NewErrCodeMsg(TokenParseError, "解析错误")
	DbErr                = NewErrCodeMsg(DbError, "数据库错误")
	DbQueryErr           = NewErrCodeMsg(DbQueryError, "查询出错")
	DbLastInsertIdErr    = NewErrCodeMsg(DbLastInsertIdError, "查询失败")
	DbInsertErr          = NewErrCodeMsg(DbInsertError, "创建失败")
	DbNotFoundErr        = NewErrCodeMsg(DbNotFoundError, "记录不存在")
	DbAlreadyExistsErr   = NewErrCodeMsg(DbAlreadyExistsError, "记录已经存在")
	ParamRequiredErr     = NewErrCodeMsg(ParamRequiredError, "缺少参数")
	DbUpdateErr          = NewErrCodeMsg(DbUpdateError, "更新失败")
	SqlBuildErr          = NewErrCodeMsg(SqlBuildError, "sql构造错误")
	MarshalErr           = NewErrCodeMsg(MarshalError, "解析错误")
	CacheExpireErr       = NewErrCodeMsg(CacheExpireError, "缓存已过期")
	CacheErr             = NewErrCodeMsg(CacheError, "缓存错误")
	LoginTimeoutErr      = NewErrCodeMsg(LoginTimeout, "登录超时")
	SignedInElsewhereErr = NewErrCodeMsg(SignedInElseWhere, "在其他地方登录")
	RequiredTokenErr     = NewErrCodeMsg(RequiredTokenError, "未认证")
	CaptchaErr           = NewErrCodeMsg(CaptchaError, "验证码验证失败")
	CacheNotFoundErr     = NewErrCodeMsg(CacheNotFound, "不存在")
	DBDeleteErr          = NewErrCodeMsg(DbDeleteError, "删除错误")
)

// NewDbNotFoundErr label表示具体对象的名称，例如:label=138999234 错误消息:138999234不存在
func NewDbNotFoundErr(label string) *CodeError {
	return NewErrCodeMsg(DbNotFoundError, fmt.Sprintf("%s不存在", label))
}

// NewDbInsertError label表示创建具体对象的名称，例如:msg=用户 错误消息:用户创建失败
func NewDbInsertError(label string) *CodeError {
	return NewErrCodeMsg(DbInsertError, fmt.Sprintf("%s创建错误", label))
}

// NewDbAlreadyExistsErr label表示创建具体对象的名称，例如:msg=张三 错误消息:张三已经存在
func NewDbAlreadyExistsErr(label string) *CodeError {
	return NewErrCodeMsg(DbAlreadyExistsError, fmt.Sprintf("%s已经存在", label))
}

// NewParamRequiredErr label表示创建具体对象的名称，例如:msg=名称 错误消息:名称必填
func NewParamRequiredErr(label string) *CodeError {
	return NewErrCodeMsg(ParamRequiredError, fmt.Sprintf("%s必填", label))
}

// NewDbUpdateErr 更新失败
func NewDbUpdateErr(msg string) *CodeError {
	return NewErrCodeMsg(DbUpdateError, msg)
}

// NewTokenParserErr Token解析失败
func NewTokenParserErr(msg string) *CodeError {
	return NewErrCodeMsg(TokenParseError, msg)
}

// NewCacheExpiredErr 缓存过期错误
func NewCacheExpiredErr(msg string) *CodeError {
	return NewErrCodeMsg(CacheExpireError, msg)
}

// NewCacheErr 缓存
func NewCacheErr(msg string) *CodeError {
	return NewErrCodeMsg(CacheError, msg)
}

// NewSqlBuildErr sql构造错误
func NewSqlBuildErr(msg string) *CodeError {
	return NewErrCodeMsg(SqlBuildError, msg)
}

// NewRecordDisabledErr label表示创建具体对象的名称，例如:msg=名称 错误消息:名称必填
func NewRecordDisabledErr(label string) *CodeError {
	return NewErrCodeMsg(RecordDisabled, fmt.Sprintf("%s禁用", label))
}

// NewDBDeleteError 删除错误
func NewDBDeleteError(label string) *CodeError {
	return NewErrCodeMsg(DbDeleteError, label)
}

// NewDbQueryErr 数据库查询错误
func NewDbQueryErr(label string) *CodeError {
	return NewErrCodeMsg(DbQueryError, label)
}

func NewBizInfoError(msg string) *CodeError {
	return NewErrCodeMsg(BizInfoError, msg)
}
